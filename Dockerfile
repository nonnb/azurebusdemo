﻿FROM microsoft/dotnet:2.0-sdk AS build-env
WORKDIR /app

# copy everything else and build
COPY . ./
RUN dotnet restore AMQP.Demo/*.csproj
RUN dotnet publish AMQP.Demo/*.csproj -c Release -o out

# build runtime image
FROM microsoft/dotnet:2.0-runtime 
WORKDIR /app
COPY --from=build-env /app/AMQP.Demo/out .
ENTRYPOINT ["dotnet", "AMQP.Demo.dll"]