﻿using System;

namespace AMQP.Common
{
    public interface IMessageConsumer : IDisposable
    {
        /// <summary>
        /// Subscribe, and start listening immediately
        /// </summary>
        void Subscribe(string topic, string filter, Action<byte[]> callback);
        /// <summary>
        /// Stop listening
        /// </summary>
        void Stop();
    }
}