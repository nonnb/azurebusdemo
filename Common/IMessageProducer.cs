﻿using System;
using System.Threading.Tasks;

namespace AMQP.Common
{
    public interface IMessageProducer : IDisposable
    {
        Task Produce(string topic, string routingInfo, byte[] message);
    }
}