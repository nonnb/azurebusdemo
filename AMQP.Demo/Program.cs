﻿using AMQP.AzureServiceBus;
using AMQP.Common;
using AMQP.RabbitMQ;
using System;
using System.Text;
using System.Threading.Tasks;

namespace AMQP.Demo
{
    public enum AmqpType
    {
        RabbitMq,
        AzureServiceBus
    }
    public enum ProduceOrConsume
    {
        Produce,
        Consume
    }

    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 2
                || !Enum.TryParse<ProduceOrConsume>(args[0], true, out var produceConsumeType)
                || !Enum.TryParse<AmqpType>(args[1], true, out var amqpType))
            {
                Console.WriteLine($"Usage: dotnet AmqpDemo {{produce|consume}} {{rabbitmq|azureservicebus}}");
                return;
            }
            Console.WriteLine($"{produceConsumeType} to/from {amqpType}");

            const string NoticesTopic = "notices";

            if (produceConsumeType == ProduceOrConsume.Consume)
            {
                using (var importantConsumer = CreateConsumer(amqpType))
                using (var infoConsumer = CreateConsumer(amqpType))
                {
                    importantConsumer.Subscribe(NoticesTopic, "important",
                        msg =>
                        {
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.WriteLine($"Important notice received: {Encoding.UTF8.GetString(msg)}");
                            Console.ResetColor();
                        });

                    infoConsumer.Subscribe(NoticesTopic, "info",
                        msg =>
                        {
                            Console.BackgroundColor = ConsoleColor.Blue;
                            Console.WriteLine($"Info notice received: {Encoding.UTF8.GetString(msg)}");
                            Console.ResetColor();
                        });

                    Console.WriteLine("Consumers set up ... Enter to Quit");
                    Console.ReadLine();
                }
            }
            else
            {
                var publisher = CreateProducer(amqpType);

                Task.WhenAll(
                        publisher.Produce(NoticesTopic, "important", Encoding.UTF8.GetBytes("Important Announcement")),
                        publisher.Produce(NoticesTopic, "info", Encoding.UTF8.GetBytes("Elvis left the building")),
                        publisher.Produce(NoticesTopic, "junkmail", Encoding.UTF8.GetBytes("Canary for sale - going cheep!")))
                    .GetAwaiter()
                    .GetResult();
            }
        }

        const string rabbitHost = "YourRabbitHost";
        const int rabbitPort = 5672;
        const string rabbitUser = "guest";
        const string rabbitPass = "guest"; // That's the default in Rabbit, change it!
        const string azureSbConnString = "Endpoint=sb://xxx/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=xxx";

        static IMessageConsumer CreateConsumer(AmqpType amqpType)
        {
            return amqpType == AmqpType.RabbitMq
                ? (IMessageConsumer)new RabbitConsumer(rabbitHost, 5672, rabbitUser, rabbitPass)
                : new AsbConsumer(azureSbConnString);
        }

        static IMessageProducer CreateProducer(AmqpType amqpType)
        {
            return amqpType == AmqpType.RabbitMq
                ? (IMessageProducer)new RabbitProducer(rabbitHost, 5672, rabbitUser, rabbitPass)
                : new AsbProducer(azureSbConnString);
        }
    }
}
