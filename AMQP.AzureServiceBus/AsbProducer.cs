﻿using System;
using System.Threading.Tasks;
using AMQP.Common;
using Microsoft.Azure.ServiceBus;

namespace AMQP.AzureServiceBus
{
    public class AsbProducer : IMessageProducer
    {
        private readonly string _serviceBusConnectionString;

        public AsbProducer(string serviceBusConnectionString)
        {
            _serviceBusConnectionString = serviceBusConnectionString;
        }

        public async Task Produce(string topic, string routingInfo, byte[] message)
        {
            // TODO ... can create a cached dictionary of topic clients ...
            var topicClient = new TopicClient(_serviceBusConnectionString, topic);
            var azureMessage = new Message(message)
            {
                CorrelationId = Guid.NewGuid().ToString(),
                Label = routingInfo
                // Can also route based on 'Headers' provided in UserProperties
                //UserProperties =
                //{
                //    ["RoutingInfo"] = routingInfo
                //}
            };
            await topicClient.SendAsync(azureMessage)
                .ConfigureAwait(false);
        }

        public void Dispose()
        {
            // Nothing to dispose, really
        }
    }
}