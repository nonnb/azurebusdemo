﻿using System;
using AMQP.Common;
using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;

namespace AMQP.AzureServiceBus
{
    public class AsbConsumer : IMessageConsumer
    {
        private readonly string _serviceBusConnectionString;
        private volatile bool _mustStop = false;

        public AsbConsumer(string serviceBusConnectionString)
        {
            _serviceBusConnectionString = serviceBusConnectionString;
        }

        public void Stop()
        {
            _mustStop = true;
        }

        public void Subscribe(string topic, string filter, Action<byte[]> callback)
        {
            var topicClient = new TopicClient(_serviceBusConnectionString, topic);

            // Because we rely on existing subscriptions, I've assumed the subscription name is the same as the filter
            var sbConnection = new ServiceBusConnectionStringBuilder(_serviceBusConnectionString)
            {
                EntityPath = $"notices/subscriptions/{filter}",
                TransportType = TransportType.AmqpWebSockets
            };
            // var subscriptionClient = new SubscriptionClient(sbConnection, filter, ReceiveMode.ReceiveAndDelete);
            var subscriptionClient = new SubscriptionClient(sbConnection, filter, ReceiveMode.ReceiveAndDelete);
            var oldRules = subscriptionClient.GetRulesAsync()
                .GetAwaiter()
                .GetResult();

            foreach(var rule in oldRules)
            {
                subscriptionClient.RemoveRuleAsync(rule.Name)
                    .GetAwaiter()
                    .GetResult();
            }
            subscriptionClient.AddRuleAsync(new RuleDescription
            {
                Filter = new CorrelationFilter { Label = filter },
                Name = $"{filter}Rule"
            })
            .GetAwaiter()
            .GetResult();

            var consumer = new Microsoft.Azure.ServiceBus.Core.MessageReceiver(sbConnection, ReceiveMode.ReceiveAndDelete);

            Task.Run(async () =>
            {
                while (true && !_mustStop)
                {
                    var receivedMessage = await consumer.ReceiveAsync(TimeSpan.FromSeconds(1));
                    if (receivedMessage != null)
                    {
                        callback(receivedMessage.Body);
                    }
                }
            });
        }

        public void Dispose()
        {
            Stop();
        }

        //    _serviceBusConnectionString,
        //    EntityNameHelper.FormatSubscriptionPath(topic, filter),
        //    ReceiveMode.ReceiveAndDelete);


        //using Microsoft.Azure.Management.ServiceBus;
        //using Microsoft.Azure.Management.ServiceBus.Models;
        //using Microsoft.Azure.Management.ResourceManager;
        //using Microsoft.IdentityModel.Clients.ActiveDirectory;
        //using Microsoft.Rest;

        //    private async Task CreateConsumerSubscription(string topic, string filter)
        //    {
        //        // https://stackoverflow.com/questions/49421196/
        //        var tenantId = "97465cd9-5fc8-4f0f-9cb5-3e9bb623c612";
        //        var clientId = "faa02b57-a63b-4020-897c-b77d05cfb9ee";
        //        var clientSecret = "nbNjT17jT/s33Bi35Dmy0vU6cPG0x8nn+CFHdfCOV5c=";
        //        var context = new AuthenticationContext($"https://login.microsoftonline.com/{tenantId}");
        //        var resourceGroupName = "rg-stubuntu";
        //        var namespaceName = "ctazure";
        //        var token = await context.AcquireTokenAsync(
        //            "https://management.core.windows.net/",
        //            new ClientCredential(clientId, clientSecret)
        //        );
        //        var creds = new TokenCredentials(token.AccessToken);
        //        var sbClient = new ServiceBusManagementClient(creds)
        //        {
        //            SubscriptionId = "cd2d7cb9-3509-4aae-aeb4-7208c7c9c991"
        //        };
        //        var subscriptionParams = new SBSubscription
        //        {
        //            MaxDeliveryCount = 10
        //        };

        //        var subs = await sbClient.Subscriptions.CreateOrUpdateAsync(
        //            resourceGroupName, namespaceName, topic, $"Subs-{filter}", subscriptionParams);
        //        // TODO : Now we need to create a subscription filter!
        //    }
    }
}