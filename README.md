# AzureAMQPDemo

(Demo given to the Azure Cape Town meetup)

The goal of this code sample is to provide a simple abstraction layer (Common : `IMessageConsumer` and `IMessageProducer`) which can be used to target either RabbitMq or Azure Service Bus.

 - For RabbitMQ, you need to create an exchange called `notices`
 - For AzureServiceBus, you need to create a Topic called `notices`, and two subscription queues named `important` and `notices`.

Build project `AMQP.Demo` from the command line `dotnet publish -c Release`

 - Rabbit operates in Pub / Sub mode (i.e. multiple consumer, each creating a queue and each receiving a copy of subscribed messages)
 - Azure SB operates in Competing Consumer mode - since only one subscription queue receives each message, consumers will fight for each message.