﻿using AMQP.Common;
using RabbitMQ.Client;
using System;
using System.Threading.Tasks;

namespace AMQP.RabbitMQ
{
    public class RabbitProducer : IMessageProducer
    {
        private readonly IConnection _connection;

        public RabbitProducer(string hostName, int port, string username, string password)
        {
            var factory = new ConnectionFactory()
            {
                HostName = hostName,
                Port = port,
                UserName = username,
                Password = password
            };
            _connection = factory.CreateConnection();
        }

        public Task Produce(string topic, string routingInfo, byte[] message)
        {
            using (var channel = _connection.CreateModel())
            {
                channel.BasicPublish(
                    exchange: topic,
                    routingKey: routingInfo,
                    body: message);
            }
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            if (_connection != null)
            {
                if (_connection.IsOpen)
                    _connection.Close();
            }
            _connection.Dispose();
        }
    }
}
