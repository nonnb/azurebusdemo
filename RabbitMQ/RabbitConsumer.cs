﻿using AMQP.Common;
using RabbitMQ.Client;
using System;
using RabbitMQ.Client.Events;

namespace AMQP.RabbitMQ
{
    public class RabbitConsumer : IMessageConsumer
    {
        private readonly IConnection _connection;
        private IModel _channel;

        public RabbitConsumer(string hostName, int port, string username, string password)
        {
            var factory = new ConnectionFactory
            {
                HostName = hostName,
                Port = port,
                UserName = username,
                Password = password
            };
            _connection = factory.CreateConnection();
        }

        public void Subscribe(string topic, string filter, Action<byte[]> callback)
        {
            _channel = _connection.CreateModel();
            var queueName = $"Consumer-{filter}-{Guid.NewGuid()}";
            _channel.QueueDeclare(queue: queueName,
                                    durable: false,
                                    exclusive: false,
                                    autoDelete: true,
                                    arguments: null);
            _channel.QueueBind(queueName, topic, filter);

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (model, eventArgs) =>
            {
                callback?.Invoke(eventArgs.Body);
                _channel.BasicAck(deliveryTag: eventArgs.DeliveryTag, multiple: false);
            };
            _channel.BasicConsume(queue: queueName,
                                  autoAck: false,
                                  consumer: consumer);
        }

        public void Stop()
        {
            if (_channel.IsOpen)
            {
                _channel.Close();
            }
        }

        public void Dispose()
        {
            if (_channel != null)
            {
                _channel.Dispose();
            }
            if (_connection != null)
            {
                _connection.Dispose();
            }
        }
    }
}
